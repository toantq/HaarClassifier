Để chạy được mã nguồn, cần cài đặt python 2.7, thư viện opencv 3.20 và ứng dụng Jupyter notebook.
- Cài đặt Annaconda version 2.7 sẽ tích hợp được cả python 2.7 và Jupyter notebook: https://www.anaconda.com/download/#windows
- Cài đặt thêm thư viện opencv 3.20
- Sau khi cài đặt hoàn tất, đặt thư mục HaarClassifier (chứa mã nguồn) vào thư mục Jupyter notebook đã cài đặt trong ổ C.
- Mở cmd, gõ lệnh: jupyter notebook, sau đó tìm đến mã nguồn và chạy.